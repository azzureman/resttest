package io.azzu.services;


import io.azzu.App;
import io.azzu.models.Account;
import io.azzu.models.AccountOperation;
import io.azzu.repositories.interfaces.AccountRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:test.properties")
@Sql({ "classpath:init-data.sql" })
public class BankRestControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

//    @Before
//    public void setup() throws Exception {
//        this.mockMvc = webAppContextSetup(webApplicationContext).build();
//        accountRepository =  mock(AccountRepository.class);
//    }

    @Test
    public void testCreateAccount() throws Exception {
        ResponseEntity<Void> responseEntity = restTemplate.exchange(
            "/bankaccount/{id}",
            HttpMethod.POST,
            null,
            Void.class,
            12345
        );

        int status = responseEntity.getStatusCodeValue();

        assertEquals("Incorrect Response Status", HttpStatus.CREATED.value(), status);
    }

    @Test
    public void testCreateAccountAlreadyExist() throws Exception {
        ResponseEntity<Void> responseEntity = restTemplate.exchange(
            "/bankaccount/{id}",
            HttpMethod.POST,
            null,
            Void.class,
            11111
        );

        int status = responseEntity.getStatusCodeValue();
        assertEquals("Incorrect Response Status", HttpStatus.CONFLICT.value(), status);
    }

    @Test
    public void testCreateAccountBadRequest() throws Exception {

        ResponseEntity<Void> responseEntity;
        int status;

        responseEntity = restTemplate.exchange(
            "/bankaccount/{id}",
            HttpMethod.POST,
            null,
            Void.class,
            123450
        );

        status = responseEntity.getStatusCodeValue();
        assertEquals("Incorrect Response Status", HttpStatus.BAD_REQUEST.value(), status);

        responseEntity = restTemplate.exchange(
            "/bankaccount/{id}",
            HttpMethod.POST,
            null,
            Void.class,
            "abcde"
        );

        status = responseEntity.getStatusCodeValue();
        assertEquals("Incorrect Response Status", HttpStatus.BAD_REQUEST.value(), status);

        responseEntity = restTemplate.exchange(
            "/bankaccount/{id}",
            HttpMethod.POST,
            null,
            Void.class,
            ""
        );

        status = responseEntity.getStatusCodeValue();
        assertEquals("Incorrect Response Status", HttpStatus.NOT_FOUND.value(), status);
    }

    @Test
    public void testDepositToBankAccount() throws Exception {
        AccountOperation accountOperation = new AccountOperation();
        accountOperation.setAmount(100L);

        HttpEntity<AccountOperation> requestEntity = new HttpEntity<AccountOperation>(accountOperation);

        ResponseEntity<Void> responseEntity = restTemplate.exchange(
            "/bankaccount/{id}/deposit",
            HttpMethod.PUT,
            requestEntity,
            Void.class,
            22222
        );

        int status = responseEntity.getStatusCodeValue();
        assertEquals("Incorrect Response Status", HttpStatus.OK.value(), status);
    }

    @Test
    public void testDepositToBankAccountNegativeValue() throws Exception {

        AccountOperation accountOperation = new AccountOperation();
        accountOperation.setAmount(-100L);

        HttpEntity<AccountOperation> requestEntity = new HttpEntity<AccountOperation>(accountOperation);

        ResponseEntity<Void> responseEntity = restTemplate.exchange(
            "/bankaccount/{id}/deposit",
            HttpMethod.PUT,
            requestEntity,
            Void.class,
            22222
        );

        int status = responseEntity.getStatusCodeValue();
        assertEquals("Incorrect Response Status", HttpStatus.NOT_ACCEPTABLE.value(), status);
    }

    @Test
    public void testDepositToBankAccountBadRequest() throws Exception {
        ResponseEntity<Void> responseEntity = restTemplate.exchange(
            "/bankaccount/{id}/deposit",
            HttpMethod.PUT,
            null,
            Void.class,
            22222
        );

        int status = responseEntity.getStatusCodeValue();
        assertEquals("Incorrect Response Status", HttpStatus.BAD_REQUEST.value(), status);
    }

    @Test
    public void testDepositToBankAccountNotExistBalance() throws Exception {
        AccountOperation accountOperation = new AccountOperation();
        accountOperation.setAmount(100L);

        HttpEntity<AccountOperation> requestEntity = new HttpEntity<AccountOperation>(accountOperation);

        ResponseEntity<Void> responseEntity = restTemplate.exchange(
            "/bankaccount/{id}/deposit",
            HttpMethod.PUT,
            requestEntity,
            Void.class,
            23456
        );

        int status = responseEntity.getStatusCodeValue();
        assertEquals("Incorrect Response Status", HttpStatus.NOT_FOUND.value(), status);
    }

    @Test
    public void testWithdrawFromBankAccount() throws Exception {
        AccountOperation accountOperation = new AccountOperation();
        accountOperation.setAmount(100L);

        HttpEntity<AccountOperation> requestEntity = new HttpEntity<AccountOperation>(accountOperation);

        ResponseEntity<Void> responseEntity = restTemplate.exchange(
            "/bankaccount/{id}/withdraw",
            HttpMethod.PUT,
            requestEntity,
            Void.class,
            33333
        );

        int status = responseEntity.getStatusCodeValue();
        assertEquals("Incorrect Response Status", HttpStatus.OK.value(), status);
    }

    @Test
    public void testWithdrawTooMachFromBankAccount() throws Exception {
        AccountOperation accountOperation = new AccountOperation();
        accountOperation.setAmount(500L);

        HttpEntity<AccountOperation> requestEntity = new HttpEntity<AccountOperation>(accountOperation);

        ResponseEntity<Void> responseEntity = restTemplate.exchange(
            "/bankaccount/{id}/withdraw",
            HttpMethod.PUT,
            requestEntity,
            Void.class,
            33333
        );

        int status = responseEntity.getStatusCodeValue();
        assertEquals("Incorrect Response Status", HttpStatus.NOT_ACCEPTABLE.value(), status);
    }

    @Test
    public void testWithdrawFromBankAccountNegativeValue() throws Exception {
        AccountOperation accountOperation = new AccountOperation();
        accountOperation.setAmount(-100L);

        HttpEntity<AccountOperation> requestEntity = new HttpEntity<AccountOperation>(accountOperation);

        ResponseEntity<Void> responseEntity = restTemplate.exchange(
            "/bankaccount/{id}/withdraw",
            HttpMethod.PUT,
            requestEntity,
            Void.class,
            33333
        );

        int status = responseEntity.getStatusCodeValue();
        assertEquals("Incorrect Response Status", HttpStatus.NOT_ACCEPTABLE.value(), status);
    }

    @Test
    public void testWithdrawFromBankAccountBadRequest() throws Exception {
        ResponseEntity<Void> responseEntity = restTemplate.exchange(
            "/bankaccount/{id}/deposit",
            HttpMethod.PUT,
            null,
            Void.class,
            33333
        );

        int status = responseEntity.getStatusCodeValue();
        assertEquals("Incorrect Response Status", HttpStatus.BAD_REQUEST.value(), status);
    }

    @Test
    public void testWithdrawFromBankAccountNotExistBalance() throws Exception {
        AccountOperation accountOperation = new AccountOperation();
        accountOperation.setAmount(100L);

        HttpEntity<AccountOperation> requestEntity = new HttpEntity<AccountOperation>(accountOperation);

        ResponseEntity<Void> responseEntity = restTemplate.exchange(
            "/bankaccount/{id}/deposit",
            HttpMethod.PUT,
            requestEntity,
            Void.class,
            34567
        );
    }

    @Test
    public void testGetBankAccountBalance() throws Exception {
        ResponseEntity<AccountOperation> responseEntity  = restTemplate.getForEntity(
            "/bankaccount/{id}/balance",
            AccountOperation.class,
            44444
        );

        int status = responseEntity.getStatusCodeValue();
        AccountOperation accountOperation = responseEntity.getBody();

        assertEquals("Incorrect Response Status", HttpStatus.OK.value(), status);

        assertNotNull(accountOperation);
        assertEquals(400L, accountOperation.getAmount());
    }

    @Test
    public void testGetBankAccountBalanceBadRequest() throws Exception {
        ResponseEntity<AccountOperation> responseEntity  = restTemplate.getForEntity(
            "/bankaccount/{id}/balance",
            AccountOperation.class,
            "abcdef"
        );

        int status = responseEntity.getStatusCodeValue();
        assertEquals("Incorrect Response Status", HttpStatus.BAD_REQUEST.value(), status);
    }

    @Test
    public void testGetBankAccountNotExistBalance() throws Exception {
        ResponseEntity<AccountOperation> responseEntity  = restTemplate.getForEntity(
            "/bankaccount/{id}/balance",
            AccountOperation.class,
            45678
        );

        int status = responseEntity.getStatusCodeValue();
        assertEquals("Incorrect Response Status", HttpStatus.NOT_FOUND.value(), status);
    }
}
