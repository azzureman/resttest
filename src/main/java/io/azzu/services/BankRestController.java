package io.azzu.services;

import io.azzu.models.AccountOperation;
import io.azzu.repositories.interfaces.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
public class BankRestController {

    private AccountRepository repository;

    BankRestController(AccountRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(value = "/bankaccount/{id}", method = RequestMethod.POST)
    public ResponseEntity<Void> createBankAccaunt(@PathVariable("id") int id, 	UriComponentsBuilder ucBuilder) {

        if (id < 0 || id > 99999) return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        if (repository.isAccountExist(id)) return new ResponseEntity<Void>(HttpStatus.CONFLICT);

        repository.createAccount(id);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

//    Или id всегда должно передоваться 5ю цифрами, и потоэтому нужно его в виде строки принимать,
//    проверять длину (5), и после проверять каждый символ, что это цифра. И только потом уже парсить в int?
//    private boolean checkId(String id) {
//        if (id.length() != 5) return false;
//        for (char c : id.toCharArray()) {
//            if (c < '0' || c > '9') return false;
//        }
//        return true;
//    }

    @RequestMapping(value = "/bankaccount/{id}/deposit", method = RequestMethod.PUT)
    public ResponseEntity<Void> depositToBankAccount(@PathVariable("id") int id, @RequestBody AccountOperation accountOperation) {

        if (id < 0 || id > 99999) return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        if (!repository.isAccountExist(id)) return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);

        if (accountOperation.getAmount() < 0) return new ResponseEntity<Void>(HttpStatus.NOT_ACCEPTABLE);

        repository.depositIntoAccount(id, accountOperation.getAmount());
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/bankaccount/{id}/withdraw", method = RequestMethod.PUT)
    public ResponseEntity<Void> withdrawFromBankAccount(@PathVariable("id") int id, @RequestBody AccountOperation accountOperation) {

        if (id < 0 || id > 99999) return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        if (!repository.isAccountExist(id)) return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);

        if (accountOperation.getAmount() < 0) return new ResponseEntity<Void>(HttpStatus.NOT_ACCEPTABLE);

        long balance = repository.getAccountBalance(id);
        if (balance < accountOperation.getAmount()) return new ResponseEntity<Void>(HttpStatus.NOT_ACCEPTABLE);

        repository.withdrawFromAccount(id, accountOperation.getAmount());
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/bankaccount/{id}/balance", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AccountOperation> getBankAccountBalance(@PathVariable("id") int id) {

        if (id < 0 || id > 99999) return new ResponseEntity<AccountOperation>(HttpStatus.BAD_REQUEST);
        if (!repository.isAccountExist(id)) return new ResponseEntity<AccountOperation>(HttpStatus.NOT_FOUND);

        AccountOperation balance = new AccountOperation();
        balance.setAmount(repository.getAccountBalance(id));
        return new ResponseEntity<AccountOperation>(balance, HttpStatus.OK);
    }




}
