package io.azzu.repositories;

import io.azzu.models.Account;
import io.azzu.repositories.interfaces.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;

@Repository
public class PostgresAccountRepository implements AccountRepository {

    private JdbcTemplate template;
    private Set<Integer> accountIdsCache;

    public PostgresAccountRepository() {
        accountIdsCache = new HashSet<>();
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    @Override
    public Account getAccountById(int id) {
        return template.queryForObject("SELECT * FROM accounts WHERE account_id = ?",
            (ResultSet rs, int rowNum) -> new Account(rs.getInt("account_id"), rs.getLong("balance")),
            id
        );
    }

    @Override
    public void createAccount(int id) {
        Account account = new Account(id);
        template.update("INSERT INTO accounts (account_id, balance) VALUES (?, ?)", account.getId(), account.getBalance());
        accountIdsCache.add(account.getId());
    }

    @Override
    public boolean isAccountExist(int id) {
        if (accountIdsCache.contains(id)) return true;
        boolean exist = !template.query(
            "SELECT * FROM accounts WHERE account_id = ?",
            (ResultSet rs, int rowNum) -> new Account(rs.getInt("account_id"), rs.getLong("balance")),
            id
        ).isEmpty();
        if (exist) accountIdsCache.add(id);
        return exist;
    }

    @Override
    public long getAccountBalance(int id) {
        return template.queryForObject(
            "SELECT balance FROM accounts WHERE account_id = ?",
            (ResultSet rs, int rowNum) -> rs.getLong("balance"),
            id
        );
    }

    @Override
    public void depositIntoAccount(int id, long value) {
        template.update("UPDATE accounts SET balance = balance + ? WHERE account_id = ?", value, id);
    }

    @Override
    public void withdrawFromAccount(int id, long value) {
        template.update("UPDATE accounts SET balance = balance - ? WHERE account_id = ?", value, id);
    }

}
