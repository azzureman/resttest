package io.azzu.models;

public class Account {

    private final int id;
    private long balance;

    public Account(int id) {
        this(id, 0);
    }

    public Account(int id, long balance) {
        this.id = id;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

}
