package io.azzu.repositories.interfaces;

import io.azzu.models.Account;

public interface AccountRepository {
    Account getAccountById(int id);
    void createAccount(int id);
    boolean isAccountExist(int id);
    long getAccountBalance(int id);
    void depositIntoAccount(int id, long value);
    void withdrawFromAccount(int id, long value);
}
