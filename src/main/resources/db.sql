CREATE TABLE IF NOT EXISTS accounts (
  id          SERIAL PRIMARY KEY,
  account_id  INT NOT NULL UNIQUE,
  balance     BIGINT NOT NULL
);